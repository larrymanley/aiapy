0.5.2 (2021-05-24)
==================

Bug Fixes
---------

- Pin sunpy to below version 3.1 or higher.

0.5.1 (2021-05-24)
==================

Backwards Incompatible Changes
------------------------------

- Pin sunpy dependency to ``<=3.0`` to allow for backwards compatibility with ``search_metadata``. (`#111 <https://gitlab.com/LMSAL_HUB/aia_hub/aiapy/-/merge_requests/111>`__)

0.5.0 (2021-04-09)
==================

Features
--------

- Add a flag to :func:`aiapy.psf.deconvolve` that sets negative intensity values to zero before performing the deconvolution. (`#107 <https://gitlab.com/LMSAL_HUB/aia_hub/aiapy/-/merge_requests/107>`__)

0.4.0 (2020-12-10)
==================

Features
--------

- Added a function (:func:`~aiapy.util.sdo_location`) to obtain the SDO location at a given time. (`#57 <https://gitlab.com/LMSAL_HUB/aia_hub/aiapy/-/merge_requests/57>`__)
- Added a function (:func:`~aiapy.calibrate.respike`) for reinserting hot pixels into level 1 images. (`#62 <https://gitlab.com/LMSAL_HUB/aia_hub/aiapy/-/merge_requests/62>`__)
- Updated default calibration version to 10.
  Added test for version 10 (`#90 <https://gitlab.com/LMSAL_HUB/aia_hub/aiapy/-/merge_requests/90>`__)

Bug Fixes
---------

- Updated default calibration version number for degradation correction.
  Added tests for multiple calibration versions (`#74 <https://gitlab.com/LMSAL_HUB/aia_hub/aiapy/-/merge_requests/74>`__)
- Fixed a bug where an out of date calibration epoch was used if there were older duplicate versions available in the same epoch. (`#90 <https://gitlab.com/LMSAL_HUB/aia_hub/aiapy/-/merge_requests/90>`__)
- `aiapy.calibrate.util.get_pointing_table` now raises a more user-friendly `RuntimeError` if no pointing information can be found during the requested times.
  Previously it would raise a `KeyError`. (`#91 <https://gitlab.com/LMSAL_HUB/aia_hub/aiapy/-/merge_requests/91>`__)
- `aiapy.calibrate.update_pointing` now searches 12 hours either side of the map date for pointing information.
  This allows for some very rare instances where more than 3 hours elapses between pointing information updates. (`#91 <https://gitlab.com/LMSAL_HUB/aia_hub/aiapy/-/merge_requests/91>`__)

0.3.2 (2020-11-29)
==================

No significant changes.

0.3.1 (2020-11-15)
==================

Features
--------

- :func:`aiapy.calibrate.register` now raises a warning if the level number is missing or is greater than 1. (`#94 <https://gitlab.com/LMSAL_HUB/aia_hub/aiapy/-/merge_requests/94>`__)

0.3.0 (2020-10-06)
==================

Features
--------

- Added a function (:func:`~aiapy.calibrate.normalize_exposure`) to normalize an image by its exposure time. (`#78 <https://gitlab.com/LMSAL_HUB/aia_hub/aiapy/-/merge_requests/78>`__)
- :func:`~aiapy.calibrate.degradation` can now accept `~astropy.time.Time` objects with length greater than 1.
  This makes it easier to compute the channel degradation over long intervals. (`#80 <https://gitlab.com/LMSAL_HUB/aia_hub/aiapy/-/merge_requests/80>`__)
- Citation information for `aiapy` is now available from `aiapy.__citation__`. (`#82 <https://gitlab.com/LMSAL_HUB/aia_hub/aiapy/-/merge_requests/82>`__)
- The pointing table can now be passsed in as a keyword argument to :func:`~aiapy.calibrate.update_pointing`.
  Added a :func:`~aiapy.calibrate.util.get_pointing_table` to retrieve the 3-hour pointing table from JSOC over a given time interval. (`#84 <https://gitlab.com/LMSAL_HUB/aia_hub/aiapy/-/merge_requests/84>`__)

Bug Fixes
---------

- The ``CROTA2`` keyword update in :func:`~aiapy.calibrate.update_pointing` now includes the value of ``SAT_ROT`` from the FITS header.
  Previously, the keyword was only being updated with ``INSTROT``. (`#84 <https://gitlab.com/LMSAL_HUB/aia_hub/aiapy/-/merge_requests/84>`__)

0.2.0 (2020-07-16)
==================

Features
--------

- Functionality for respiking level 1 images and fetching spike data from JSOC
- Updated calibration data now fetched from JSOC to account for instrument degradation
- Compatibility fix with sunpy > 2.0.0 which previously caused level 1.5 maps to expand by several pixels
- Functionality for fetching the location of SDO in time

0.1.0  (2020-03-31)
===================

Features
--------

- Update pointing keywords in the header using the 3-hour pointing values from the JSOC
- Correct Heliographic Stonyhurst observer location
- Register images by removing the roll angle, centering the image, and scaling to a common resolution (i.e. "aia_prep")
- Calculate wavelength response functions for all channels, including time-dependent effects
- Account for channel degradation in image correction
- Compute the point spread function and deconvolve an image with the point spread function (with optional GPU acceleration)
